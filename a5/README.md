> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Java Servlet Pages

## Juan Abreut

### Assignment 5 Requirements:

*Sub-Heading:*

1. Create Servlet interacting with database
2. Prevent Cross side Scripting
3. Establish connections using connection pool

#### README.md file should include the following items:

> This is my blockquote.
> 
> This is the second paragraph in the blockquote as an example.
>
> #### Git commands w/short descriptions:

1. git init - Initializes repo
2. git status - Checks current status of repo
3. git add - Adds current changes to que for a push
4. git commit - Commits change of local repo
5. git push - Pushes changes to the server
6. git pull - pulls changes from online repo
7. git branch -lists all branches

#### Assignment Screenshots:

*First Application Screen:

![Carousel](img/a5_1.png)

*Second Screenshot:

![Valid form](img/a5_2.png)

*Second Screenshot:

![Valid form](img/a5_3.png)



[Local Host](http://localhost/repos/LIS4368)
