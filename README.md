# LIS4368 - Java Servlet Pages

## Juan Abreut

### Assignments: 
 
* [In this repo: A1 README.md](a1)

    * Install Tomcat

    * Install JDK

    * Provide screenshots of installations

    * Create Bitbucket repo

    * Complete Bitbucket tutorials (see bottom)
	
* [In this repo: A2 README.md](a2)
	* Screenshot of Tomcat hello page
	
	* Screenshot of Tomcat index page 
	
	* Screenshot of Querybook
	
* [In this repo: A3 README.md](a3)
	* Screenshot of reverse engineered ERD
	
	* Inclusion of DB SQL File with 10 inserts
	
	* Inclusion of MWB file
	
* [In this repo: P1 README.md](p1)
	* Client Side Validation
	
	* Inclusion of Carousel
	
	* Error and Confirmation on form

* [In this repo: A4 README.md](a4)
	* Screenshot of reverse engineered ERD
	
	* Inclusion of DB SQL File with 10 inserts
	
	* Inclusion of MWB file
	
* [In this repo: A5 README.md](a5)

	* Create Servlet interacting with database

	* Prevent Cross side Scripting

	* Establish connections using connection pool
	
* [In this repo: P2 README.md](p2)

	* Create Servlet interacting with database

	* Enable Delte and update functionality

	* Show changes from database side

* [BitBucketStationLocations Tutorial](https://bitbucket.org/Peaceall/bitbucketstationlocations)
 
* [Myteamquotes Tutorial](https://bitbucket.org/Peaceall/myteamquotes)