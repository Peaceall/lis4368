> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Application Development

## Juan Abreut

### Assignment 2 Requirements:

*Sub-Heading:*

1. To show version Control Mastery
2. Show Developmental Installation Success
3. Chapter questions

#### README.md file should include the following items:

* Screenshot of Tomcat hello page
* Screenshot of Tomcat index page 
* Screenshot of Querybook

> This is my blockquote.
> 
> This is the second paragraph in the blockquote as an example.
>
> #### Git commands w/short descriptions:

1. git init - Initializes repo
2. git status - Checks current status of repo
3. git add - Adds current changes to que for a push
4. git commit - Commits change of local repo
5. git push - Pushes changes to the server
6. git pull - pulls changes from online repo
7. git branch -lists all branches

#### Assignment Screenshots:

*LINK: localhost:9999/hello*

[AMPPS localhost:9999/hello](http://localhost:9999/hello)

*Screenshot of running java Hello*:

[JDK Installation creenshot](http://localhost:9999/hello/sayhello)

*link*:

[JDK Installation Screenshot](http://localhost:9999/hello/querybook.html)


*Screenshot query*:

![JDK Installation Screenshot](./img/a2_1.png)



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Peaceall/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/Peaceall/myteamquotes/ "My Team Quotes Tutorial")
